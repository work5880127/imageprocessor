//
// Created by timofey on 25.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_EDGE_DETECTOR_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_EDGE_DETECTOR_H_

#include "filter.h"
#include "grayscale_filter.h"

class EdgeDetector : public Filter {
public:
    explicit EdgeDetector(uint8_t threshold);
    Image operator()(const Image& image) const override;

private:
    const MatrixFilter make_smooth_{{{0, -1, 0}, {-1, 4, -1}, {0, -1, 0}}};
    const GrayscaleFilter make_uniform_ = GRAYSCALE_FILTER;

private:
    uint8_t threshold_ = 0;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_EDGE_DETECTOR_H_
