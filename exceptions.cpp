//
// Created by timofey on 14.03.23.
//

#include "exceptions.h"

ImageProcessorException::ImageProcessorException(std::string message) : data_(std::move(message)) {
}

const char *ImageProcessorException::what() const noexcept {
    return data_.c_str();
}

ParserException::ParserException(std::string message) : ImageProcessorException(message) {
}

InvalidArgumentException::InvalidArgumentException() : ParserException("type or value of arguments are incorrect") {
}

InvalidArgumentException::InvalidArgumentException(std::string message) : ParserException(message) {
}
