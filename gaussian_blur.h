//
// Created by timofey on 26.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_GAUSSIAN_BLUR_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_GAUSSIAN_BLUR_H_

#include "filter.h"

class GaussianBlur : public Filter {
public:
    explicit GaussianBlur(double sigma);
    Image operator()(const Image& image) const override;

private:
    double Calculate(int32_t dx) const;

    double sigma_ = 0;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_GAUSSIAN_BLUR_H_
