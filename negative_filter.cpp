//
// Created by timofey on 25.03.23.
//

#include "negative_filter.h"

Image NegativeFilter::operator()(const Image& image) const {
    Image result(image.Height(), image.Width());
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            const Pixel current = image(x, y);
            Pixel& result_pixel = result(x, y);
            result_pixel.b = std::numeric_limits<uint8_t>::max() - current.b;
            result_pixel.g = std::numeric_limits<uint8_t>::max() - current.g;
            result_pixel.r = std::numeric_limits<uint8_t>::max() - current.r;
        }
    }
    return result;
}
