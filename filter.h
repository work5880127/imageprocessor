//
// Created by timofey on 14.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_FILTER_H_

#include "image.h"

class Filter {
public:
    virtual Image operator()(const Image& image) const = 0;
    virtual ~Filter() = default;
};

class MatrixFilter : public Filter {
public:
    explicit MatrixFilter(std::vector<std::vector<double>> matrix);
    Image operator()(const Image& image) const override;

protected:
    std::vector<std::vector<double>> matrix_;  // from the top-left corner, access with [y][x]
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_FILTER_H_
