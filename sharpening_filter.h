//
// Created by timofey on 25.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_SHARPENING_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_SHARPENING_FILTER_H_

#include "filter.h"

const MatrixFilter SHARPENING_FILTER({{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}});

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_SHARPENING_FILTER_H_
