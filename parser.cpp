//
// Created by timofey on 14.03.23.
//

#include "parser.h"
#include "exceptions.h"

#include "crop_filter.h"
#include "sharpening_filter.h"
#include "grayscale_filter.h"
#include "edge_detector.h"
#include "negative_filter.h"
#include "saturation_filter.h"
#include "gaussian_blur.h"
#include "draw_filter.h"

#include <filesystem>
#include <iostream>

std::vector<std::shared_ptr<Filter>> Parser::FromCommandLine(int argc, char** argv) {
    const ParserException not_enough = ParserException("Not enough arguments");

    std::vector<std::shared_ptr<Filter>> res;
    std::vector<std::string> commands;
    commands.reserve(argc);
    for (int i = 0; i < argc; ++i) {
        commands.emplace_back(argv[i]);
    }

    for (size_t i = 3; i < commands.size(); ++i) {
        std::string_view command = commands[i];
        if (command == "-sharp") {
            res.push_back(std::make_shared<MatrixFilter>(SHARPENING_FILTER));
        } else if (command == "-gs") {
            res.push_back(std::make_shared<GrayscaleFilter>(GRAYSCALE_FILTER));
        } else if (command == "-edge") {
            if (commands.size() <= i + 1) {
                throw not_enough;
            }
            ++i;
            res.push_back(std::make_shared<EdgeDetector>(Parser::ToUInt8(commands[i])));
        } else if (command == "-neg") {
            res.push_back(std::make_shared<NegativeFilter>(NEGATIVE_FILTER));
        } else if (command == "-crop") {
            if (commands.size() <= i + 2) {
                throw not_enough;
            }
            ++i;
            size_t width = Parser::ToUInt32(commands[i]);
            ++i;
            size_t height = Parser::ToUInt32(commands[i]);
            res.push_back(std::make_shared<CropFilter>(width, height));
        } else if (command == "-saturation") {
            if (commands.size() <= i + 1) {
                throw not_enough;
            }
            ++i;
            res.push_back(std::make_shared<SaturationFilter>(Parser::ToDouble(commands[i])));
        } else if (command == "-blur") {
            if (commands.size() < i + 1) {
                throw not_enough;
            }
            ++i;
            res.push_back(std::make_shared<GaussianBlur>(Parser::ToDouble(commands[i])));
        }else if (command == "-draw") {
            if (commands.size() < i + 1) {
                throw not_enough;
            }
            ++i;
            res.push_back(std::make_shared<DrawFilter>(Parser::ToUInt32(commands[i])));
        } else if (command[0] == '-') {
            std::string answer = "no such filter '";
            throw ParserException(answer + commands[i] + '\'');
        } else {
            throw ParserException("Too much arguments");
        }
    }
    return res;
}

void Parser::Handle(std::exception* e) {
    std::cout << e->what() << std::endl;
}

uint8_t Parser::ToUInt8(std::string& s) {
    if (s == "0") {
        return 0;
    }
    if (s == "1") {
        return std::numeric_limits<uint8_t>::max();
    }
    if (s.size() < 2) {
        throw InvalidArgumentException();
    }
    if (s[0] == '1' && s[1] == '.' && std::all_of(s.begin() + 2, s.end(), [](unsigned char c) { return c == '0'; })) {
        return std::numeric_limits<uint8_t>::max();
    }
    if (s[0] != '0' || s[1] != '.' ||
        !std::all_of(s.begin() + 2, s.end(), [](unsigned char c) { return std::isdigit(c); })) {
        throw InvalidArgumentException("Value should be a floating point number between 0 and 1");
    }
    return static_cast<uint8_t>(stod(s) * std::numeric_limits<uint8_t>::max());
}

uint32_t Parser::ToUInt32(std::string& s) {
    if (!std::all_of(s.begin(), s.end(), [](unsigned char c) { return std::isdigit(c); })) {
        throw InvalidArgumentException("Value should be an integer");
    }
    if (s.size() > std::to_string(std::numeric_limits<uint32_t>::max()).size()) {
        throw InvalidArgumentException("Value is too big");
    }
    uint64_t value = stoll(s);
    if (value > std::numeric_limits<uint32_t>::max()) {
        throw InvalidArgumentException("Value is too big");
    }
    return value;
}

double Parser::ToDouble(std::string& s) {
    try {
        return stod(s);
    } catch (std::exception& e) {
        throw InvalidArgumentException(std::string("Can`t convert to double: ") + e.what());
    }
}
