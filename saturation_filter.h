//
// Created by timofey on 26.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_SATURATION_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_SATURATION_FILTER_H_

#include "filter.h"
#include "grayscale_filter.h"

class SaturationFilter : public Filter {
public:
    explicit SaturationFilter(double coefficient);
    Image operator()(const Image& image) const override;

private:
    GrayscaleFilter intensity_filter_ = GRAYSCALE_FILTER;
    double coefficient_ = 0;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_SATURATION_FILTER_H_
