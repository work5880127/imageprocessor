//
// Created by timofey on 14.03.23.
//

#include "image.h"

#include "math.h"

Image::Image(std::vector<std::vector<Pixel>> data) : data_(std::move(data)){};

Image::Image(size_t height, size_t width) {
    data_.reserve(width);
    for (size_t i = 0; i < width; ++i) {
        data_.emplace_back(height);
    }
}

Pixel& Image::operator()(size_t x, size_t y) {
    return data_[x][y];
}

const Pixel& Image::operator()(size_t x, size_t y) const {
    return data_[x][y];
}

Pixel& Image::operator()(std::pair<size_t, size_t> coordinates) {
    return (*this)(coordinates.first, coordinates.second);
}

const Pixel& Image::operator()(std::pair<size_t, size_t> coordinates) const {
    return (*this)(coordinates.first, coordinates.second);
}

size_t Image::Height() const {
    return data_.front().size();
}

size_t Image::Width() const {
    return data_.size();
}

bool Pixel::operator==(const Pixel& other) const {
    return r == other.r && g == other.g && b == other.b;
}

bool Image::operator==(const Image& other) const {
    return data_ == other.data_;
}
