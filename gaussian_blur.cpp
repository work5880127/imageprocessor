//
// Created by timofey on 26.03.23.
//

#include "gaussian_blur.h"

namespace {
const int32_t MEANINGFUL_DISTANCE_MULTIPLIER = 3;
}  // namespace

GaussianBlur::GaussianBlur(double sigma) : sigma_(sigma) {
}

double GaussianBlur::Calculate(int32_t dx) const {
    return exp(-static_cast<double>(dx) * dx / (2 * sigma_ * sigma_)) / sqrt(2 * std::numbers::pi * sigma_ * sigma_);
}

Image GaussianBlur::operator()(const Image& image) const {
    std::vector<std::vector<double>> horizontal_matrix(1);
    std::vector<std::vector<double>> vertical_matrix;
    int32_t distance_to_side = std::ceil(std::abs(sigma_) * MEANINGFUL_DISTANCE_MULTIPLIER);
    horizontal_matrix[0].reserve(distance_to_side * 2 + 1);
    vertical_matrix.reserve(distance_to_side * 2 + 1);
    for (int32_t dx = -distance_to_side; dx <= distance_to_side; ++dx) {
        double value = Calculate(dx);
        horizontal_matrix[0].push_back(value);
        vertical_matrix.push_back({value});
    }
    MatrixFilter vertical(vertical_matrix);
    MatrixFilter horizontal(horizontal_matrix);
    return horizontal(vertical(image));
}
