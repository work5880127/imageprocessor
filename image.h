//
// Created by timofey on 14.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_IMAGE_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_IMAGE_H_

#include <vector>
#include <string>
#include <limits>
#include <cmath>

#pragma pack(push, 1)
// sizeof(Pixel) should be 3
struct Pixel {
    uint8_t b = 0;
    uint8_t g = 0;
    uint8_t r = 0;

    bool operator==(const Pixel& other) const;

    template <typename T>
    static uint8_t ForceBound(T x) {
        return std::clamp(static_cast<int32_t>(std::round(x)),
                          static_cast<int32_t>(std::numeric_limits<uint8_t>::min()),
                          static_cast<int32_t>(std::numeric_limits<uint8_t>::max()));
    }
};
#pragma pack(pop)

class Image {
public:
    struct ImageSize {
        size_t width;
        size_t height;
    };

public:
    explicit Image(std::vector<std::vector<Pixel>> data = {});
    Image(size_t height, size_t width);

    // granting multidimensional item lookup, same as operator[] in C++23
    Pixel& operator()(size_t x, size_t y);
    const Pixel& operator()(size_t x, size_t y) const;

    Pixel& operator()(std::pair<size_t, size_t> coordinates);
    const Pixel& operator()(std::pair<size_t, size_t> coordinates) const;

public:
    size_t Width() const;
    size_t Height() const;

public:
    bool operator==(const Image& other) const;

private:
    std::vector<std::vector<Pixel>> data_;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_IMAGE_H_
