//
// Created by timofey on 25.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_CROP_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_CROP_FILTER_H_

#include "filter.h"

class CropFilter : public Filter {
public:
    CropFilter(size_t new_width, size_t new_height);
    Image operator()(const Image& image) const override;

private:
    size_t new_width_ = 0;
    size_t new_height_ = 0;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_CROP_FILTER_H_
