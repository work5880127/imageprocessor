//
// Created by timofey on 26.03.23.
//

#include "saturation_filter.h"

#include "negative_filter.h"
#include "exceptions.h"

SaturationFilter::SaturationFilter(double coefficient) {
    coefficient_ = coefficient;
}

Image SaturationFilter::operator()(const Image& image) const {
    Image brightness = intensity_filter_(image);
    if (image.Height() * image.Width() > std::numeric_limits<uint64_t>::max() / std::numeric_limits<uint8_t>::max()) {
        throw ImageProcessorException("Size of the image is too big for filter '-saturation'");
    }
    uint64_t total = 0;
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            total += brightness(x, y).r;
        }
    }
    uint8_t average_brightness = Pixel::ForceBound(static_cast<long double>(total) / (image.Width() * image.Height()));
    Image result(image.Height(), image.Width());
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            Pixel& current = result(x, y);
            const Pixel image_pixel = image(x, y);
            current.r = Pixel::ForceBound((image_pixel.r - average_brightness) * coefficient_ + average_brightness);
            current.g = Pixel::ForceBound((image_pixel.g - average_brightness) * coefficient_ + average_brightness);
            current.b = Pixel::ForceBound((image_pixel.b - average_brightness) * coefficient_ + average_brightness);
        }
    }
    return result;
}
