//
// Created by timofey on 14.03.23.
//

#include "filter.h"

#include <limits>
#include <math.h>

namespace {
template <typename T, typename R, typename proxy_type = int64_t>
T SafeSum(T x, R y, proxy_type low = std::numeric_limits<T>::min(), proxy_type high = std::numeric_limits<T>::max()) {
    proxy_type proxy = static_cast<proxy_type>(x) + static_cast<proxy_type>(y);
    return std::clamp(proxy, low, high);
}
}  // namespace

MatrixFilter::MatrixFilter(std::vector<std::vector<double>> matrix) : matrix_(matrix) {
}

Image MatrixFilter::operator()(const Image &image) const {
    Image result(image.Height(), image.Width());
    auto w = static_cast<int64_t>(image.Width());
    auto h = static_cast<int64_t>(image.Height());
    auto zero = static_cast<int64_t>(0);

    int64_t centered_matrix_height = static_cast<int64_t>(matrix_.size()) / 2;
    int64_t centered_matrix_width = static_cast<int64_t>(matrix_.front().size()) / 2;
    for (size_t x = 0; x < image.Width(); ++x) {
        for (size_t y = 0; y < image.Height(); ++y) {
            double b = 0.0;
            double g = 0.0;
            double r = 0.0;
            for (int64_t dy = -centered_matrix_height; dy <= centered_matrix_height; ++dy) {
                for (int64_t dx = -centered_matrix_width; dx <= centered_matrix_width; ++dx) {
                    const Pixel image_pixel = image(SafeSum(x, dx, zero, w - 1), SafeSum(y, dy, zero, h - 1));
                    const double coefficient = matrix_[centered_matrix_height + dy][centered_matrix_width + dx];
                    b += image_pixel.b * coefficient;
                    g += image_pixel.g * coefficient;
                    r += image_pixel.r * coefficient;
                }
            }
            result(x, y) = Pixel{Pixel::ForceBound(b), Pixel::ForceBound(g), Pixel::ForceBound(r)};
        }
    }
    return result;
}
