//
// Created by timofey on 27.03.23.
//

#include "draw_filter.h"
#include "grayscale_filter.h"

DrawFilter::DrawFilter(int32_t number_colors_important) {
    number_colors_important_ = number_colors_important;
}

Image DrawFilter::operator()(const Image &image) const {
    Image gs = GRAYSCALE_FILTER(image);
    int32_t values_per_color = std::numeric_limits<uint8_t>::max() / number_colors_important_;
    Image result(image.Height(), image.Width());
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            Pixel& result_pixel = result(x, y);
            uint8_t value = (gs(x, y).b / values_per_color) * values_per_color + values_per_color / 2;
            result_pixel.b = value;
            result_pixel.g = value;
            result_pixel.r = value;
        }
    }
    return result;
}
