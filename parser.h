//
// Created by timofey on 14.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_PARSER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_PARSER_H_

#include "filter.h"

#include <vector>
#include <memory>
#include <optional>
#include <exception>

class Parser {
public:
    static std::vector<std::shared_ptr<Filter>> FromCommandLine(int argc, char** argv);

public:
    static uint8_t ToUInt8(std::string& s);
    static uint32_t ToUInt32(std::string& s);
    static double ToDouble(std::string& s);

public:
    static void Handle(std::exception* e);
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_PARSER_H_
