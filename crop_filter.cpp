//
// Created by timofey on 25.03.23.
//

#include "crop_filter.h"

#include "exceptions.h"

CropFilter::CropFilter(size_t new_width, size_t new_height) {
    if (new_height * new_width == 0) {
        throw ImageProcessorException("image size can`t be 0");
    }
    new_height_ = new_height;
    new_width_ = new_width;
}

Image CropFilter::operator()(const Image& image) const {
    size_t new_height = std::min(new_height_, image.Height());
    size_t new_width = std::min(new_width_, image.Width());
    Image result(new_height, new_width);
    auto h = image.Height();
    for (size_t dy = 1; dy <= new_height; ++dy) {
        for (size_t x = 0; x < new_width; ++x) {
            result(x, new_height - dy) = image(x, h - dy);
        }
    }
    return result;
}
