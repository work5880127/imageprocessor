//
// Created by timofey on 25.03.23.
//

#include "grayscale_filter.h"

GrayscaleFilter::GrayscaleFilter(double b_coefficient, double g_coefficient, double r_coefficient) {
    b_coefficient_ = b_coefficient;
    g_coefficient_ = g_coefficient;
    r_coefficient_ = r_coefficient;
}

Image GrayscaleFilter::operator()(const Image& image) const {
    Image result = Image(image.Height(), image.Width());
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            const Pixel image_pixel = image(x, y);
            double gs =
                image_pixel.r * r_coefficient_ + image_pixel.g * g_coefficient_ + image_pixel.b * b_coefficient_;
            uint8_t gs_value = Pixel::ForceBound(gs);
            Pixel& result_pixel = result(x, y);
            result_pixel.r = gs_value;
            result_pixel.g = gs_value;
            result_pixel.b = gs_value;
        }
    }
    return result;
}
