//
// Created by timofey on 20.03.23.
//

#include "bmp_file.h"
#include "exceptions.h"

#include "fstream"
#include "limits"

bool BMPInterface::FileHeader::IsValid() const {
    return file_type == BMP_FILE_TYPE;
}

bool BMPInterface::InfoHeader::IsValid() const {
    return planes == 1 && bit_count == ACCEPTABLE_BIT_COUNT && compression == 0 && colors_used == 0 &&
           colors_important == 0 && width * height != 0;
}

uint32_t BMPInterface::Align(int32_t width) {
    return (width * 3 + 3) / 4 * 4;
}

uint32_t BMPInterface::Offset() {
    return sizeof(FileHeader) + sizeof(InfoHeader);
}

uint32_t BMPInterface::Size(const Image& image) {
    return Align(static_cast<int32_t>(image.Width())) * image.Height() + Offset();
}

Image BMPInterface::Load(const std::string& path) {
    std::ifstream input(path, std::ios::in | std::ios::binary);
    if (!input) {
        throw ImageProcessorException("Can't open file");
    }

    // loading and processing headers
    FileHeader file_header;
    InfoHeader info_header;
    input.read(reinterpret_cast<char*>(&file_header), sizeof(file_header));
    if (!file_header.IsValid()) {
        input.close();
        throw ImageProcessorException("The program can't handle this file type");
    }
    input.read(reinterpret_cast<char*>(&info_header), sizeof(info_header));
    if (!info_header.IsValid()) {
        input.close();
        throw ImageProcessorException("Input file damaged or in wrong format");
    }

    // loading image data
    Image result = Image(std::abs(info_header.height), info_header.width);
    int64_t row_stride = Align(info_header.width);
    if (info_header.height > 0) {
        for (int32_t y = 0; y < info_header.height; ++y) {
            input.seekg(file_header.offset_data + row_stride * y, std::ios::beg);
            for (int32_t x = 0; x < info_header.width; ++x) {
                input.read(reinterpret_cast<char*>(&result(x, y)), sizeof(Pixel));
            }
        }
    } else {
        for (int32_t y = -info_header.height - 1; y > -1; --y) {
            input.seekg(file_header.offset_data + row_stride * (info_header.height - 1 - y), std::ios::beg);
            for (int32_t x = 0; x < info_header.width; ++x) {
                input.read(reinterpret_cast<char*>(&result(x, y)), sizeof(Pixel));
            }
        }
    }
    input.close();
    return result;
}

void BMPInterface::Save(const Image& image, const std::string& path) {
    std::ofstream output(path, std::ios::out | std::ios::binary);
    if (!output) {
        throw ImageProcessorException("Can't open file");
    }
    if (size_t max_size = std::numeric_limits<int32_t>::max(); image.Height() > max_size || image.Width() > max_size) {
        output.close();
        throw ImageProcessorException("Can't save an image that size");
    }

    // headers
    FileHeader file_header;
    file_header.offset_data = Offset();
    file_header.file_size = Size(image);
    output.write(reinterpret_cast<char*>(&file_header), sizeof(file_header));

    InfoHeader info_header;
    info_header.height = static_cast<int32_t>(image.Height());
    info_header.width = static_cast<int32_t>(image.Width());
    info_header.size = sizeof(info_header);
    info_header.size_image = info_header.height * info_header.width;
    output.write(reinterpret_cast<char*>(&info_header), sizeof(info_header));

    // data
    uint32_t padding = Align(static_cast<int32_t>(image.Width())) - image.Width() * 3;
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            output.write(reinterpret_cast<const char*>(&image(x, y)), 3);
        }
        std::vector<char> filler(4);
        output.write(filler.data(), padding);
    }
    output.close();
}
