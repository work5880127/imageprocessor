//
// Created by timofey on 25.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_NEGATIVE_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_NEGATIVE_FILTER_H_

#include "filter.h"

class NegativeFilter : public Filter {
public:
    Image operator()(const Image& image) const override;
};

const NegativeFilter NEGATIVE_FILTER = NegativeFilter();

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_NEGATIVE_FILTER_H_
