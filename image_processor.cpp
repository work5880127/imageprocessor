#include "parser.h"
#include "exceptions.h"
#include "bmp_file.h"

#include <iostream>

int main(int argc, char **argv) {
    try {
        if (argc == 1) {
            const std::string help_message = {
                "\n"
                "usage: image_processor {path to bmp file} {path to the result} [-{filter1} [filter1_argument1] ...]] "
                "[-{filter2} [filter1_argument2] ...]] ...\n"
                "\n"
                "Filters are:\n"
                "-crop {width} {height}    Crop the image starting from the top-left corner\n"
                "-gs                       Turns the image into a black and white picture\n"
                "-neg                      Reverses colors on the picture\n"
                "-sharp                    Increases sharpness\n"
                "-edge {threshold}         Detects edges with value more than threshold\n"
                "-saturation {coefficient} Adjusts saturation of the image\n"
                "-blur {sigma}             Blurs the image according to the Gaussian function with given sigma\n"
                "-draw {number of colors}  Generalizes the gs image into a given number of colors"};
            std::cout << help_message << std::endl;
            exit(0);
        } else if (argc == 2) {
            std::cout << "Not enough arguments" << std::endl;
            return 1;
        }
        Image image = BMPInterface::Load(argv[1]);
        auto filters = Parser::FromCommandLine(argc, argv);
        for (const auto &filter : filters) {
            image = (*filter)(image);
        }
        BMPInterface::Save(image, argv[2]);
    } catch (ImageProcessorException &exception) {
        Parser::Handle(&exception);
        return 1;
    } catch (...) {
        std::cout << "something is very wrong" << std::endl;
        return 1;
    }
}