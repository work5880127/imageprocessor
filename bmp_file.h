//
// Created by timofey on 20.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_BMP_FILE_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_BMP_FILE_H_

#include "image.h"

#include <cstdint>

namespace {
const uint16_t BMP_FILE_TYPE = 0x4D42;  // Value of BM file-type
const uint16_t ACCEPTABLE_BIT_COUNT = 24;
}  // namespace

class BMPInterface {
public:
    static Image Load(const std::string& path);
    static void Save(const Image& image, const std::string& path);

private:
#pragma pack(push, 1)
    struct FileHeader {
        uint16_t file_type{BMP_FILE_TYPE};  // File type always BM which is 0x4D42
        uint32_t file_size{0};              // Size of the file (in bytes)
        uint16_t reserved1{0};              // Reserved, always 0
        uint16_t reserved2{0};              // Reserved, always 0
        uint32_t offset_data{0};            // Start position of pixel data (bytes from the beginning of the file)

        bool IsValid() const;
    };

    struct InfoHeader {
        uint32_t size{0};                          // Size of this header (in bytes)
        int32_t width{0};                          // width of bitmap in pixels
        int32_t height{0};                         // width of bitmap in pixels
                                                   // (if positive, bottom-up, with origin in lower left corner)
                                                   // (if negative, top-down, with origin in upper left corner)
        uint16_t planes{1};                        // No. of planes for the target device, this is always 1
        uint16_t bit_count{ACCEPTABLE_BIT_COUNT};  // No. of bits per pixel
        uint32_t compression{0};                   // 0 or 3 - uncompressed. Always uncompressed
        uint32_t size_image{0};                    //
        int32_t x_pixels_per_meter{0};             //
        int32_t y_pixels_per_meter{0};             //
        uint32_t colors_used{0};                   // No. color indexes in the color table. Always 0 in this program
        uint32_t colors_important{0};              // No. of colors used for displaying the bitmap. Always 0

        bool IsValid() const;
    };
#pragma pack(pop)

    static uint32_t Align(int32_t width);

    static uint32_t Offset();

    static uint32_t Size(const Image& image);
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_BMP_FILE_H_
