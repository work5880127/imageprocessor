//
// Created by timofey on 14.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_EXCEPTIONS_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_EXCEPTIONS_H_

#include <exception>
#include <string>

class ImageProcessorException : public std::exception {
public:
    explicit ImageProcessorException(std::string message);
    ~ImageProcessorException() override = default;

    const char* what() const noexcept override;

protected:
    std::string data_;
};

class ParserException : public ImageProcessorException {
public:
    explicit ParserException(std::string message);
};

class InvalidArgumentException : public ParserException {
public:
    InvalidArgumentException();
    explicit InvalidArgumentException(std::string message);
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_EXCEPTIONS_H_
