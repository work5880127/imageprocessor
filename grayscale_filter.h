//
// Created by timofey on 25.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_GRAYSCALE_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_GRAYSCALE_FILTER_H_

#include "filter.h"

class GrayscaleFilter : public Filter {
public:
    GrayscaleFilter(double b_coefficient, double g_coefficient, double r_coefficient);
    Image operator()(const Image& image) const override;

private:
    double b_coefficient_ = 0;
    double g_coefficient_ = 0;
    double r_coefficient_ = 0;
};

const GrayscaleFilter GRAYSCALE_FILTER = GrayscaleFilter(0.114, 0.587, 0.299);

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_GRAYSCALE_FILTER_H_
