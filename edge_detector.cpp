//
// Created by timofey on 25.03.23.
//

#include "edge_detector.h"

EdgeDetector::EdgeDetector(uint8_t threshold) : threshold_(threshold) {
}

Image EdgeDetector::operator()(const Image& image) const {
    Image result = make_smooth_(make_uniform_(image));
    for (size_t y = 0; y < image.Height(); ++y) {
        for (size_t x = 0; x < image.Width(); ++x) {
            Pixel& current = result(x, y);
            uint8_t value = (current.b > threshold_) ? std::numeric_limits<uint8_t>::max() : 0;
            current = Pixel{value, value, value};
        }
    }
    return result;
}
