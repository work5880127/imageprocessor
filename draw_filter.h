//
// Created by timofey on 27.03.23.
//

#ifndef CPP_HSE_TASKS_IMAGE_PROCESSOR_DRAW_FILTER_H_
#define CPP_HSE_TASKS_IMAGE_PROCESSOR_DRAW_FILTER_H_

#include "filter.h"

class DrawFilter : public Filter {
public:
    explicit DrawFilter(int32_t number_colors_important);
    Image operator()(const Image& image) const override;

private:
    int32_t number_colors_important_ = 0;
};

#endif  // CPP_HSE_TASKS_IMAGE_PROCESSOR_DRAW_FILTER_H_
